const socket = io()

// Elements
const $messageForm = document.querySelector('#message-form')
const $messageFormInput = $messageForm.querySelector('input')
const $messageFormButton = $messageForm.querySelector('button')
const $shareLocationButton = document.querySelector('#share-location')
const $messages = document.querySelector('#messages')

const { username, room } = Qs.parse(location.search, { ignoreQueryPrefix: true })

// Templates
const messageTemplate = document.querySelector('#message-template').innerHTML
const locationTemplate = document.querySelector('#location-template').innerHTML

socket.on('sendWelcomeMessage', (welcomeMessage) => {
    console.log(welcomeMessage);
})

$messageForm.addEventListener('submit', (e) => {
    e.preventDefault()
    $messageFormButton.setAttribute('disabled', true)
    const message = e.target.elements.message.value
    socket.emit('sendMessage', message, (error) => {
        $messageFormButton.removeAttribute('disabled')
        $messageFormInput.value = ''
        $messageFormInput.focus()
        if (error) {
            return console.log(error)
        }
        console.log('Message Delivered');

    })
})

socket.on('sendMessage', (message) => {
    console.log(`Message Received: ${message}`);
    const html = Mustache.render(messageTemplate, {
        message
    })
    $messages.insertAdjacentHTML('beforeend', html)
})

socket.on('locationMessage', (message) => {
    const html = Mustache.render(locationTemplate, {
        message
    })
    $messages.insertAdjacentHTML('beforeend', html)
})

$shareLocationButton.addEventListener('click', () => {
    $shareLocationButton.setAttribute('disabled', true)
    if (!navigator.geolocation) {
        return alert('Geolocation is not supported in your browser')
    }
    navigator.geolocation.getCurrentPosition((position) => {
        console.log(position);
        const coords = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        }

        socket.emit('sendLocation', coords, (acknowledgement) => {
            $shareLocationButton.removeAttribute('disabled')
            console.log(acknowledgement);

        })

    })
})

socket.emit('join', {
    username,
    room
})