const express = require('express')
const http = require('http')
const socketio = require('socket.io')
const Filter = require('bad-words')

const app = express()
const server = http.createServer(app)
const io = socketio(server)

const port = process.env.PORT
const path = require('path')

const publicDirectoryPath = path.join(__dirname, '../public')

app.use(express.static(publicDirectoryPath))

let welcomeMessage = 'Welcome to Chat App!'

io.on('connection', (socket) => {
    console.log('New Websocket Connection');

    socket.on('sendMessage', (message, callback) => {
        const filter = new Filter()
        if (filter.isProfane(message)) {
            return callback('Profanity is not allowed')
        }
        io.emit('sendMessage', message)
        callback()

    })
    socket.on('disconnect', () => {
        io.emit('sendMessage', 'A user is disconnected!')

    })

    socket.on('sendLocation', (location, callback) => {
        socket.broadcast.emit('locationMessage', `https://google.com/maps?q=${location.latitude},${location.longitude}`)
        callback('Location Shared Successfully')
    })

    socket.on('join', ({
        username,
        room
    }) => {
        socket.join(room)
        socket.emit('sendWelcomeMessage', welcomeMessage)
        socket.broadcast.to(room).emit('sendMessage', `${username} has joined!`)
    })
})


server.listen(port, () => {
    console.log("Server started and listening at " + port);
})